import turtle


def make_window(clr, ttle):
	w=turtle.Screen()
	w.bgcolor(clr)
	w.title(ttle)
	return w

def make_turtle(clr, sz):
	t=turtle.Turtle()
	t.color(clr)
	t.pensize(sz)
	return t

wn=make_window("pink", "tess and alex dancing")
tess=make_turtle("green", 60)
alex=make_turtle("red", 12)
dave=make_turtle("grey", 99)

turtle.mainloop()
