import turtle

def draw_star(t,sz):
	for i in range(5):
		t.forward(sz)
		t.left(144)

wn=turtle.Screen()
wn.bgcolor("green")

tess=turtle.Turtle()
tess.color("black")

size=100

draw_star(tess,size)

turtle.mainloop()
