import turtle

def draw_star(t,sz):
	for i in range(5):
		t.forward(sz)
		t.right(144)

	t.left(36)
	t.color("green")
	t.forward(20)

wn=turtle.Screen()
wn.bgcolor("green")
tess=turtle.Turtle()
tess.color("pink")

size=40

for i in range(5):
	draw_star(tess,size)

turtle.mainloop()


