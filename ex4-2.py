

import turtle

def draw_square(t,sz):
	for i in range(4):
		t.color("pink")
		t.forward(sz)
		t.left(90)

	t.forward(sz)
	t.color("green")
	t.forward(sz/2)
	t.left(90)
	

wn=turtle.Screen()
wn.bgcolor("green")


tess=turtle.Turtle()

size=20
tess.speed(1)
for i in range(5):
	draw_square(tess,size)
	tess.speed(1)
	tess.forward(size+size/2)
	size=size+size
	tess.left(90)
	
	
	
turtle.mainloop()
