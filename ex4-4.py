import turtle

def draw_sqr(t,sz):
	for i in range(4):
		t.forward(sz)
		t.left(90)

	t.right(18)
	#t.forward(20)

wn=turtle.Screen()
wn.bgcolor("green")
tess=turtle.Turtle()
tess.color("blue")

size=40

for i in range(30):
	#tess.speed(1)
	draw_sqr(tess,size)

turtle.mainloop()


